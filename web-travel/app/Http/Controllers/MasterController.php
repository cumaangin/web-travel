<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function home(){
        return view('layouts.master');
    }
    public function about(){
        return view('layouts.about');
    }
    public function blog(){
        return view('layouts.blog');
    }
    public function contact(){
        return view('layouts.contact');
    }
    public function destination(){
        return view('layouts.destination');
    }
}
