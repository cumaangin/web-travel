<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered col-lg-12" role="document">
	  <div class="modal-content">
		<div class="modal-body search-wrap-1 ftco-animate p ">
			<div class="container-fluid">
				<div class="row d-flex justify-content-center">
					<div class="align-items-end">
						<div class="form-group">
							<label for="#">Destination</label>
						  <div class="form-field">
						<input type="text" class="form-control" placeholder="Search place">
					  </div>
				  </div>
					</div>
				</div>
				<div class="row d-flex justify-content-center">
					<div class="align-items-end">
					<div class="form-group">
						<label for="#">Booking date</label>
						<div class="form-field">
					<input type="text" class="form-control checkin_date" placeholder="Booking Date">
				  </div>
			  </div>
				</div>
			</div>
					<div class="row d-flex justify-content-center">
						<div class="align-items-end">
							<div class="form-group">
								<label for="#">Price Limit</label>
								<div class="form-field">
								  <div class="select-wrap">
						  <select name="" id="" class="form-control">
							<option value="">$5,000</option>
							<option value="">$10,000</option>
							<option value="">$50,000</option>
							<option value="">$100,000</option>
							<option value="">$200,000</option>
							<option value="">$300,000</option>
							<option value="">$400,000</option>
							<option value="">$500,000</option>
							<option value="">$600,000</option>
							<option value="">$700,000</option>
							<option value="">$800,000</option>
							<option value="">$900,000</option>
							<option value="">$1,000,000</option>
							<option value="">$2,000,000</option>
						  </select>
						</div>
						  </div>
					  </div>
						</div>
					</div>
					
					<div class="col-lg align-self-end">
						<div class="form-group d-flex justify-content-center">
							<div class="form-field">
						<input type="submit" value="Search" class="form-control btn btn-primary">
					  </div>
				  </div>
					</div>
			</div>
		  </div>
	  </div>
	</div>
  </div>
  <!-- END MODAL -->