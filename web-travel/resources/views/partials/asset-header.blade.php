<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('vacation-master/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('vacation-master/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('vacation-master/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('vacation-master/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('vacation-master/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('vacation-master/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('vacation-master/css/style.css')}}">