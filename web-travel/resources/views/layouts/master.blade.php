<!DOCTYPE html>
<html lang="en">
  <head>
    <title>KRAMA TOUR AGENCY</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
	@include('partials.asset-header')
	<link rel="stylesheet" href="style.css">
  </head>
  <body>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a href="/" class="nav-link">Home</a></li>
				<li class="nav-item"><a href="/about" class="nav-link">About</a></li>
				<li class="nav-item"><a href="/destination" class="nav-link">Destination</a></li>
				<li class="nav-item"><a href="/blog" class="nav-link">Blog</a></li>
				<li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
				<li class="nav-item cta"><a href="/booking" class="nav-link" data-toggle="modal" data-target="#exampleModalCenter">Book Now</a></li>

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <!-- MODAL  -->
	@include('partials.modal-book')
	<!-- END MODAL -->

    <div class="hero-wrap js-fullheight" style="background-image: url('https://d99i6ad9lbm5v.cloudfront.net/uploads/image/file/8853/shutterstock_1372188773.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-9 text text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
          	<a href="https://vimeo.com/45830194" class="icon-video popup-vimeo d-flex align-items-center justify-content-center mb-4">
          		<span class="ion-ios-play"></span>
            </a>
            <p class="caps" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Travel to the any corner of the world, without going around in circles</p>
            <h1 data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Make Your Tour <span class="auto-input">Amazing</span> With Us</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-6 order-md-last heading-section pl-md-5 ftco-animate">
          	<h2 class="mb-4">It's time to start your adventure</h2>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
            A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
            <p><a href="/destination" class="btn btn-primary py-3 px-4">Search Destination</a></p>
          </div>
          <div class="col-md-6">
          	<div class="row">
          		<div class="col-md-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services d-block">
		              <div class="icon"><span class="flaticon-paragliding"></span></div>
		              <div class="media-body">
		                <h3 class="heading mb-3">Activities</h3>
		                <p>A small river named Duden flows by their place and supplies it with the necessary</p>
		              </div>
		            </div>      
		          </div>
		          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services d-block">
		              <div class="icon"><span class="flaticon-route"></span></div>
		              <div class="media-body">
		                <h3 class="heading mb-3">Travel Arrangements</h3>
		                <p>A small river named Duden flows by their place and supplies it with the necessary</p>
		              </div>
		            </div>    
		          </div>
		          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services d-block">
		              <div class="icon"><span class="flaticon-tour-guide"></span></div>
		              <div class="media-body">
		                <h3 class="heading mb-3">Private Guide</h3>
		                <p>A small river named Duden flows by their place and supplies it with the necessary</p>
		              </div>
		            </div>      
		          </div>
		          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services d-block">
		              <div class="icon"><span class="flaticon-map"></span></div>
		              <div class="media-body">
		                <h3 class="heading mb-3">Location Manager</h3>
		                <p>A small river named Duden flows by their place and supplies it with the necessary</p>
		              </div>
		            </div>      
		          </div>
          	</div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-counter img" id="section-counter">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 d-flex">
    				<div class="img d-flex align-self-stretch" style="background-image:url({{asset('vacation-master/images/about.jpg')}});"></div>
    			</div>
    			<div class="col-md-6 pl-md-5 py-5">
    				<div class="row justify-content-start pb-3">
		          <div class="col-md-12 heading-section ftco-animate">
		            <h2 class="mb-4">Make Your Tour Memorable and Safe With Us</h2>
		            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>
	

		<section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Best Place Destination</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://img.inews.co.id/files/inews_new/2020/01/26/patung_cheng_ho_di_sam_po_kong.JPG');">
        				<div class="text">
        					<h3>Sam Poo Kong</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://awsimages.detik.net.id/community/media/visual/2019/12/14/7dab2b98-85f3-4ed7-84b3-d86dab58357e_169.jpeg?w=780&q=80');">
        				<div class="text">
        					<h3>Lawang Sewu</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://i1.wp.com/dosenwisata.com/wp-content/uploads/2019/09/MAJT.jpg?resize=612%2C460');">
        				<div class="text">
        					<h3>Mesjid Agung</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://sikidang.com/wp-content/uploads/Harga-Tiket-Masuk-Pantai-Marina.jpg');">
        				<div class="text">
        					<h3>Pantai Marina</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        </div>
    	</div>
    </section>

    <section class="ftco-section ftco-no-pt">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Tour Destination</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://idetrips.com/wp-content/uploads/2018/12/sam-poo-kong.jpeg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Sam Poo Kong</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://cdn-2.tstatic.net/travel/foto/bank/images/lawang-sewu_20160902_194015.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Lawang Sewu</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://1.bp.blogspot.com/-PAXVqm7Irpg/V1zWXaB0C3I/AAAAAAAAAs0/-4hZDRwdHUsFclNuK5baGhcqfWgV-zzCACLcB/s1600/masjid-agung-jateng.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Mesjid Agung</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://www.pinhome.id/blog/wp-content/uploads/2018/08/bosniatravel-net.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Brown Canyon</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://1.bp.blogspot.com/-uQE-KUip9x8/XDd6znhzBAI/AAAAAAAARx0/9OZ-4itobIYgx5KHpFJbX5TxQVF4y_45gCLcBGAs/s1600/DSCF9509.JPG');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Goa Kreo</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://awsimages.detik.net.id/community/media/visual/2019/08/27/7f7d7964-0327-489c-b598-0f7f7833de92_169.jpeg?w=700&q=90');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Kota Lama</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://museumnusantara.com/wp-content/uploads/2020/04/gereja-blenduk-oleh-museum-nusantara.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Gereja Blenduk</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://semarangsekarang.com/asset/foto_berita/tmp-cam-6297833884231551406.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Grand Maerakaca</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://wisatalova.com/wp-content/uploads/2016/10/Curug-Lawe-09.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Curug Lawe</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://jejakpiknik.com/wp-content/uploads/2017/11/@vallriz-e1575757706262.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Budhagaya</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://t-2.tstatic.net/tribunnewswiki/foto/bank/images/Taman-Wilis.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Taman Wilis</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://asset.kompas.com/crops/WBq2E5FWBo2BY-gmIlJkLg1m1bs=/0x0:780x390/780x390/data/photo/2018/12/20/1563278594.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Benteng Pandem</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
    	</div>
    </section>

    <section class="ftco-section">
      <div class="container">
      	<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Recent Post</h2>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry justify-content-end">
              <a href="blog-single.html" class="block-20" style="background-image: url({{asset('vacation-master/images/image_1.jpg')}});">
              </a>
              <div class="text mt-3 float-right d-block">
              	<div class="d-flex align-items-center mb-4 topp">
              		<div class="one">
              			<span class="day">21</span>
              		</div>
              		<div class="two">
              			<span class="yr">2019</span>
              			<span class="mos">August</span>
              		</div>
              	</div>
                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry justify-content-end">
              <a href="blog-single.html" class="block-20" style="background-image: url({{asset('vacation-master/images/image_2.jpg')}});">
              </a>
              <div class="text mt-3 float-right d-block">
              	<div class="d-flex align-items-center mb-4 topp">
              		<div class="one">
              			<span class="day">21</span>
              		</div>
              		<div class="two">
              			<span class="yr">2019</span>
              			<span class="mos">August</span>
              		</div>
              	</div>
                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url({{asset('vacation-master/images/image_3.jpg')}});">
              </a>
              <div class="text mt-3 float-right d-block">
              	<div class="d-flex align-items-center mb-4 topp">
              		<div class="one">
              			<span class="day">21</span>
              		</div>
              		<div class="two">
              			<span class="yr">2019</span>
              			<span class="mos">August</span>
              		</div>
              	</div>
                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- FOOTER -->
	@include('partials.master-footer')
	<!-- END FOOTER  -->
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  @include('partials.asset-footer')
  <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
  <script>
	  var typed = new Typed(".auto-input",{
		  strings: ["Amazing","Comfortable"], typeSpeed : 100, backSpeed: 100, loop : true
	  });
  </script>
  </body>
</html>