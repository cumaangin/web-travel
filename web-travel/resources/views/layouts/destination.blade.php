<!DOCTYPE html>
<html lang="en">
  <head>
    <title>KRAMA TOUR AGENCY</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
	@include('partials.asset-header')

  </head>
  <body>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="/about" class="nav-link">About</a></li>
                <li class="nav-item active"><a href="/destination" class="nav-link">Destination</a></li>
                <li class="nav-item"><a href="/blog" class="nav-link">Blog</a></li>
                <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
    
    <!-- MODAL -->
    @include('partials.modal-book')
    <!-- END MODAL -->

    <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('https://wallpaperaccess.com/full/472182.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <h1 class="mb-3 bread">Places to Travel</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About us <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Best Place Destination</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://img.inews.co.id/files/inews_new/2020/01/26/patung_cheng_ho_di_sam_po_kong.JPG');">
        				<div class="text">
        					<h3>Sam Poo Kong</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://awsimages.detik.net.id/community/media/visual/2019/12/14/7dab2b98-85f3-4ed7-84b3-d86dab58357e_169.jpeg?w=780&q=80');">
        				<div class="text">
        					<h3>Lawang Sewu</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://i1.wp.com/dosenwisata.com/wp-content/uploads/2019/09/MAJT.jpg?resize=612%2C460');">
        				<div class="text">
        					<h3>Mesjid Agung</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<div class="project-destination">
        			<a href="#" class="img" style="background-image: url('https://sikidang.com/wp-content/uploads/Harga-Tiket-Masuk-Pantai-Marina.jpg');">
        				<div class="text">
        					<h3>Pantai Marina</h3>
        				</div>
        			</a>
        		</div>
        	</div>
        </div>
    	</div>
    </section>

    <section class="ftco-section ftco-no-pb ftco-no-pt">
    	<div class="container">
	    	<div class="row">
					<div class="col-md-12 mb-5">
						<div class="search-wrap-1 search-wrap-notop ftco-animate p-4">
							<form action="#" class="search-property-1">
		        		<div class="row">
		        			<div class="col-lg align-items-end">
		        				<div class="form-group">
		        					<label for="#">Destination</label>
		          				<div class="form-field">
		          					<div class="icon"><span class="ion-ios-search"></span></div>
				                <input type="text" class="form-control" placeholder="Search place">
				              </div>
			              </div>
		        			</div>
		        			<div class="col-lg align-items-end">
		        				<div class="form-group">
		        					<label for="#">Booking date</label>
		        					<div class="form-field">
		          					<div class="icon"><span class="ion-ios-calendar"></span></div>
				                <input type="text" class="form-control checkin_date" placeholder="Booking Date">
				              </div>
			              </div>
		        			</div>
		        			<div class="col-lg align-items-end">
		        				<div class="form-group">
		        					<label for="#">Price Limit</label>
		        					<div class="form-field">
		          					<div class="select-wrap">
		                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
		                      <select name="" id="" class="form-control">
		                        <option value="">$5,000</option>
		                        <option value="">$10,000</option>
		                        <option value="">$50,000</option>
		                        <option value="">$100,000</option>
		                        <option value="">$200,000</option>
		                        <option value="">$300,000</option>
		                        <option value="">$400,000</option>
		                        <option value="">$500,000</option>
		                        <option value="">$600,000</option>
		                        <option value="">$700,000</option>
		                        <option value="">$800,000</option>
		                        <option value="">$900,000</option>
		                        <option value="">$1,000,000</option>
		                        <option value="">$2,000,000</option>
		                      </select>
		                    </div>
				              </div>
			              </div>
		        			</div>
		        			<div class="col-lg align-self-end">
		        				<div class="form-group">
		        					<div class="form-field">
				                <input type="submit" value="Search" class="form-control btn btn-primary">
				              </div>
			              </div>
		        			</div>
		        		</div>
		        	</form>
		        </div>
					</div>
	    	</div>
	    </div>
    </section>


    <section class="ftco-section ftco-no-pt">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Tour Destination</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://idetrips.com/wp-content/uploads/2018/12/sam-poo-kong.jpeg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Sam Poo Kong</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://cdn-2.tstatic.net/travel/foto/bank/images/lawang-sewu_20160902_194015.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Lawang Sewu</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://1.bp.blogspot.com/-PAXVqm7Irpg/V1zWXaB0C3I/AAAAAAAAAs0/-4hZDRwdHUsFclNuK5baGhcqfWgV-zzCACLcB/s1600/masjid-agung-jateng.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Mesjid Agung</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://www.pinhome.id/blog/wp-content/uploads/2018/08/bosniatravel-net.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Brown Canyon</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://1.bp.blogspot.com/-uQE-KUip9x8/XDd6znhzBAI/AAAAAAAARx0/9OZ-4itobIYgx5KHpFJbX5TxQVF4y_45gCLcBGAs/s1600/DSCF9509.JPG');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Goa Kreo</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://awsimages.detik.net.id/community/media/visual/2019/08/27/7f7d7964-0327-489c-b598-0f7f7833de92_169.jpeg?w=700&q=90');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Kota Lama</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://museumnusantara.com/wp-content/uploads/2020/04/gereja-blenduk-oleh-museum-nusantara.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Gereja Blenduk</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://semarangsekarang.com/asset/foto_berita/tmp-cam-6297833884231551406.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Grand Maerakaca</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://wisatalova.com/wp-content/uploads/2016/10/Curug-Lawe-09.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Curug Lawe</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
		<div class="row">
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://jejakpiknik.com/wp-content/uploads/2017/11/@vallriz-e1575757706262.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Budhagaya</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://t-2.tstatic.net/tribunnewswiki/foto/bank/images/Taman-Wilis.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Taman Wilis</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
        			<a href="#" class="img" style="background-image: url('https://asset.kompas.com/crops/WBq2E5FWBo2BY-gmIlJkLg1m1bs=/0x0:780x390/780x390/data/photo/2018/12/20/1563278594.jpg');"></a>
        			<div class="text p-4">
        				<a href="#"><span class="price">$300/person</span></a>
        				<h3><a href="#">Benteng Pandem</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> Semarang, Indonesia</p>
        			</div>
        		</div>
        	</div>
        </div>
    	</div>
    </section>

    <footer class="ftco-footer bg-bottom" style="background-image: url(images/footer-bg.jpg);">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Vacation</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Infromation</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Online Enquiry</a></li>
                <li><a href="#" class="py-2 d-block">General Enquiries</a></li>
                <li><a href="#" class="py-2 d-block">Booking Conditions</a></li>
                <li><a href="#" class="py-2 d-block">Privacy and Policy</a></li>
                <li><a href="#" class="py-2 d-block">Refund Policy</a></li>
                <li><a href="#" class="py-2 d-block">Call Us</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Experience</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Adventure</a></li>
                <li><a href="#" class="py-2 d-block">Hotel and Restaurant</a></li>
                <li><a href="#" class="py-2 d-block">Beach</a></li>
                <li><a href="#" class="py-2 d-block">Nature</a></li>
                <li><a href="#" class="py-2 d-block">Camping</a></li>
                <li><a href="#" class="py-2 d-block">Party</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @include('partials.asset-footer')

  </body>
</html>